#include "staticdielectricconstant.h"

#include<cmath>
#include<iostream>

using namespace std;

StaticDielectricConstant::StaticDielectricConstant(const double& rho, const double& drhodp, const double& drhodt, const double& tK)
    :
      rho_(rho),
      drhodp_(drhodp),
      drhodt_(drhodt),
      tK_(tK),
      eps0_( 1. / ( 4e-7 * M_PI * 299792458.*299792458.) ),
      alpha_(1.636e-40),
      mu_(6.138e-30),
      k_(1.380658e-23),
      na_(6.0221367e23),
      m_w_(0.018015268),
      tcrit_(647.096),
      rhocrit_(322.),
      prefactor_a( na_ * mu_ * mu_ / m_w_ / ( eps0_ * k_) ),
      prefactor_b( na_*alpha_ / m_w_ / (3. * eps0_ ) ),
      nh_{ 0.978224486826,
          -0.957771379375,
           0.237511794148,
           0.714692244396,
          -0.298217036956,
          -0.108863472196,
           0.949327488264e-1,
          -0.980469816509e-2,
           0.165167634970e-4,
           0.937359795772e-4,
          -0.123179218720e-9,
           0.196096504426e-2},
      ih_{ 1, 1, 1, 2, 3, 3, 4, 5, 6, 7, 10},
      jh_{ 0.25, 1., 2.5, 1.5, 1.5, 2.5, 2., 2., 5., 0.5, 10.}

{

}

double StaticDielectricConstant::Epsilon()
{
    double a = A();
    double b = B();
    double epsilon_{1. + a + 5.0*b };
    epsilon_ += sqrt(9. + 2.0*a + 18.0*b + a*a + 10.*a*b + 9.0*b*b );
    epsilon_ /= 4.-4.0*b;
//    cout << "StaticDielectricConstant::Epsilon() :   " << rho_ << "\t" << drhodp_ << "\t" << drhodt_ << "\t" << tK_ << endl;
    return epsilon_;
}

double StaticDielectricConstant::DEpsilonDP()
{
    const double a{A()};
    const double b{B()};
    const double dadp{dAdP()};
    const double dbdp{dBdP()};
    double depsilondp{0};

    //derivative from Maple
    depsilondp  = (-b+1.)*dadp + dbdp*(6.+a);
    depsilondp *= sqrt(a*a + (10.*b+2.)*a + 9.*(b+1.)*(b+1.) );
    depsilondp -= (b-1.)*(a+5.*b+1.)*dadp;
    depsilondp += dbdp *( (5.*a+18.)*b + a*a + 7.*a + 18.);
    depsilondp /= ( 4.0 * sqrt(a*a + (10.*b+2.)*a + 9.*(b+1.)*(b+1.)) * (b-1.)*(b-1.) );
    return depsilondp;
}

double StaticDielectricConstant::DEpsilonDT()
{
    const double a{A()};
    const double b{B()};
    const double dadt{dAdT()};
    const double dbdt{dBdT()};
    double depsilondt{0};

    //derivative from Maple
    depsilondt  = (-b+1.)*dadt + dbdt*(6.+a);
    depsilondt *= sqrt(a*a + (10.*b+2.)*a + 9.*(b+1.)*(b+1.) );
    depsilondt -= (b-1.)*(a+5.*b+1.)*dadt;
    depsilondt += dbdt *( (5.*a+18.)*b + a*a + 7.*a + 18.);
    depsilondt /= ( 4.0 * sqrt(a*a + (10.*b+2.)*a + 9.*(b+1.)*(b+1.)) * (b-1.)*(b-1.) );
    return depsilondt;
}

double StaticDielectricConstant::A(){ return prefactor_a * rho_ * g() / tK_ ; }

double StaticDielectricConstant::dAdP(){
    return prefactor_a*g()*drhodp_/tK_ + prefactor_a*rho_*dgdP()/tK_;
}

double StaticDielectricConstant::dAdT(){
    return prefactor_a*g()*drhodt_/tK_ + prefactor_a*rho_*dgdT()/tK_ - prefactor_a*rho_*g()/tK_/tK_;
}


double StaticDielectricConstant::B(){ return prefactor_b*rho_ ; }

double StaticDielectricConstant::dBdP(){ return prefactor_b*drhodp_; }

double StaticDielectricConstant::dBdT(){ return prefactor_b*drhodt_; }


double StaticDielectricConstant::g()
{
    double g_{1.};
    for(int i = 0; i<11; ++i)
    {
        g_ += nh_[i] * pow(rho_/rhocrit_,ih_[i]) * pow(tcrit_/tK_,jh_[i]);
    }
    g_ += nh_[11] * rho_/rhocrit_ * pow(tK_/228. - 1.,-1.2);
    return g_;
}

double StaticDielectricConstant::dgdP()
{
    double dgdp_{0.};
    for(int i = 0; i<11; ++i)
    {
        dgdp_ += nh_[i] * ih_[i] * pow(rho_/rhocrit_,ih_[i])/rho_ * pow(tcrit_/tK_,jh_[i]) *drhodp_;
    }
    dgdp_ += nh_[11] /rhocrit_ * pow(tK_/228. - 1.,-1.2) * drhodp_;
    return dgdp_;
}

double StaticDielectricConstant::dgdT()
{
    double dgdt_{0.};
    for(int i = 0; i<11; ++i)
    {
        dgdt_ += nh_[i] * ih_[i] * pow(rho_/rhocrit_,ih_[i])/(rho_) * pow(tcrit_/tK_,jh_[i]) * drhodt_;
        dgdt_ -= nh_[i] * pow(rho_/rhocrit_,ih_[i]) * pow(tcrit_/tK_,jh_[i]) * jh_[i] / tK_;
    }
    dgdt_ += nh_[11] * drhodt_ / rhocrit_ * pow(tK_/228. - 1.,-1.2);
    dgdt_ -= 1.2 * nh_[11] * rho_/rhocrit_ * pow(tK_/228. - 1.,-2.2) / 228.;
    return dgdt_;
}
