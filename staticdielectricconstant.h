#ifndef STATICDIELECTRICCONSTANT_H
#define STATICDIELECTRICCONSTANT_H
#include<vector>

class StaticDielectricConstant
{
public:
    StaticDielectricConstant(const double& rho, const double& drhodp, const double& drhodt, const double& tK);
    double Epsilon();
    double DEpsilonDP();
    double DEpsilonDT();

private:
    StaticDielectricConstant();
    const double& rho_;
    const double& drhodp_;
    const double& drhodt_;
    const double& tK_;

    double A();
    double dAdP(); // tested ok
    double dAdT(); // Tested ok but may carry uncertainties from g

    double B();
    double dBdP(); // tested ok
    double dBdT(); // Tested ok

    double g();
    double dgdP(); // tested ok
    double dgdT(); // tested ok (to 6th decimal)

    const double eps0_;
    const double alpha_;
    const double mu_;
    const double k_;
    const double na_;
    const double m_w_;
    const double tcrit_;
    const double rhocrit_;
    const double prefactor_a;
    const double prefactor_b;
    const std::vector<double> nh_;
    const std::vector<int> ih_;
    const std::vector<double> jh_;
};

//calculate const parts at compile time!

#endif // STATICDIELECTRICCONSTANT_H
