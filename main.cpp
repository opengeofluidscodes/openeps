#include <iostream>

#include "staticdielectricconstant.h"
#include "steam4.h"

using namespace std;

int main()
{
    double rho{1.e99};
    double drhodp{1.e99};
    double drhodt{1.e99};
    double tK{1.e99};
    const double drho{1.0e-4};
    const double dt{1.0e-4};
    double p{0.};
    double g{0.};
    double e{0.};
    double dedt{0.};
    double dedp{0.};

    Prop *prost;
    Prop *satv;
    Prop *satl;
    prost = newProp('T', 'p', 1);
    satv  = newProp('T', 'p', 1);
    satl  = newProp('T', 'p', 1);

    StaticDielectricConstant epsilon( rho, drhodp, drhodt, tK );

    int i{1};
    while(i>0)
    {
        cout << "rho = "; cin >> rho;
        cout << "tK  = "; cin >> tK;

        water_td(tK,rho,prost);
        e = epsilon.Epsilon();
        p = prost->p;

        water_td(tK,rho+drho,prost);
        double dp = prost->p - p;
        drhodp = drho/dp;
        water_tp(tK+dt,p,700.,1.0e-10,prost);
        drhodt = (prost->d - rho)/dt;
        dedt = epsilon.DEpsilonDT();
        dedp = epsilon.DEpsilonDP();
        cout.precision(12);
        tK += dt;
        double myrho = rho;
        rho = prost->d;
        cout << (epsilon.Epsilon()-e)/dt << "\t" << dedt << endl;
        tK -= dt;
        rho = myrho+drho;
        cout << (epsilon.Epsilon()-e)/dp << "\t" << dedp << endl;

    }

    freeProp(prost);
    freeProp(satv);
    freeProp(satl);
}
